package DataProcessor.DataCollectors;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.*;

public class NSETop500CollectorTest {

    NSETop500Collector nSETop500Collector;

    @BeforeClass
    public void initiateClass()
    {
        nSETop500Collector = new NSETop500Collector();
    }

    @Test
    public void testGetSymbolListIsNotEmpty() throws Exception {
        List list = nSETop500Collector.getSymbolList();
        assertTrue(list.size() > 0);
    }

    @Test
    public void testGetSymbolListIsGreaterThan498() throws Exception {
        List list = nSETop500Collector.getSymbolList();
        assertTrue(list.size() > 498);
    }

    @Test
    public void testRefreshData() throws Exception {
    }

    @Test
    public void testGetSymbolList() {
    }

    @Test
    public void testWriteSymbolsToFile() throws IOException {
        nSETop500Collector.writeSymbolsToFile(nSETop500Collector.getSymbolList());
    }
}