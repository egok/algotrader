package DataProcessor.DataCollectors;

import DomainObjects.NSEListing;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class NSETop500Collector {

    private static String NIFTY500RemoteListURL = "https://nseindia.com/content/indices/ind_nifty500list.csv";
    private URL remoteURL;
    private Map NSEListings = new ConcurrentHashMap();

    private ArrayList<String> symbolList = new ArrayList(500);

    public NSETop500Collector() {
        // Fix remote URL variable
        setRemoteURL();
        try {
            openRemoteConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        initializeDataFromRemote();
    }

    /**
     * @return ArrayList of Strings with Symbols
     */
    public ArrayList<String> getSymbolList()
    {
        return symbolList;
    }

    /**
     * Refreshes the data from RemoteÚRL
     * Gets data from the remote and reloads into the list
     */
    public void refreshData()
    {
        initializeDataFromRemote();
    }

    /**
     * Writes Symbols from the given ArrayList to file "resources/NSE500Symbols.txt"
     * @param localSymbolList ArrayList of strings containing SymbolList
     * @throws IOException
     */
    public void writeSymbolsToFile(ArrayList<String> localSymbolList) throws IOException {
        FileWriter fileWriter = new FileWriter("NSE500Symbols.txt");
        for (String symbol:localSymbolList)
        {
            fileWriter.write(symbol + "\n");
        }
        fileWriter.close();
    }

    /**
     * Gets data from the remote and loads into the list
     */
    private void initializeDataFromRemote()
    {
        try {
            readFromRemote(openRemoteConnection());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Open remote connection
     * @return InputStreamReader
     * @throws IOException
     */
    @NotNull
    private InputStreamReader openRemoteConnection() throws IOException
    {
        URLConnection urlConnection = remoteURL.openConnection();
        return new InputStreamReader(urlConnection.getInputStream());
    }

    /**
     *  Reads the list from the remote URL and stores in the NSEListings Map with Symbol as key and NSEListing Object as Value
     * @param input InputStreamReader which is returned by openRemoteConnectio/0
     * @throws IOException
     */
    private void readFromRemote(InputStreamReader input) throws IOException
    {
        BufferedReader bufferedReader = null;
        String line = "";
        String csvSplitBy = ",";

        bufferedReader = new BufferedReader(input);
        while ((line = bufferedReader.readLine())!= null)
        {
            NSEListing nseListing = new NSEListing();
            String [] listing = line.split(csvSplitBy);
            nseListing.setCompanyName(listing[0]);
            nseListing.setIndustry(listing[1]);
            nseListing.setSymbol(listing[2]);
            nseListing.setSeries(listing[3]);
            nseListing.setISINCode(listing[4]);
            //nseListing.print();

            NSEListings.put(listing[2], nseListing);
            symbolList.add(listing[2]);
        }
    }


    private void setRemoteURL()
    {
        try {
            remoteURL = new URL(NIFTY500RemoteListURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

}
