package DomainObjects;

public class NSEListing {

    private String CompanyName;
    private String Industry;
    private String Symbol;
    private String Series;
    private String ISINCode;

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getIndustry() {
        return Industry;
    }

    public void setIndustry(String industry) {
        Industry = industry;
    }

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String symbol) {
        Symbol = symbol;
    }

    public String getSeries() {
        return Series;
    }

    public void setSeries(String series) {
        Series = series;
    }

    public String getISINCode() {
        return ISINCode;
    }

    public void setISINCode(String ISINCode) {
        this.ISINCode = ISINCode;
    }

    public void print()
    {
        System.out.println("Listing "
                    +"Company Name = " + getCompanyName()
                    +", Industry = "+ getIndustry()
                    +", Symbol = " + getIndustry()
                    +", Series = "+ getSeries()
                    +", ISIN Code = "+ getISINCode());
    }
}
